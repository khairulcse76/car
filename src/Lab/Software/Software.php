<?php

namespace labApps\Lab\Software;
use PDO;

class Software {
    public $id='';
    public $unique_id='';
    public $software_title='';
    public $version='';
    public $lab='';
    public $user='root';
    public $password='';
    public $pass='';
    public $connection='';
    public $software_type='';




    public function __construct() {
        session_start();
       date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=lab', $this->user, $this->pass);
        
    }
    public function prepare($data='')
    {
       if(array_key_exists('SoftName', $data))
       {
           $this->software_title=$data['SoftName'];
       }
       if(array_key_exists('versionName', $data))
       {
           $this->version=$data['versionName'];
       }
       if(array_key_exists('type', $data))
       {
           $this->software_type=$data['type'];
       }
       
        if(array_key_exists('lab', $data))
       {
           $this->lab=$data['lab'];
       }
       if(array_key_exists('unique_id', $data))
       {
           $this->unique_id=$data['unique_id'];
       }
        if(array_key_exists('id', $data))
       {
           $this->id=$data['id'];
       }
       
        return $this;
    }
    
    public function SoftStore()
    {
       
            try {
//                 "INSERT INTO `installed_softwares` (`id`, `labinfo_id`, `software_title`, `version`, `software_type`, `created`, `updated`, `deleted`) "
//                . "VALUES (NULL, '1', 'windows', '10', 'Free', '2016-08-11 00:00:00', '2016-08-11 00:00:00', '2016-08-11 00:00:00');";
                       
                $qery = "INSERT INTO `installed_softwares` (`id`, `unique_id`, `labinfo_id`, `software_title`, `version`, `software_type`, `created`, `updated`, `deleted`)
                                         VALUES(:id, :uni, :labinfo_id, :software_title, :version, :software_type, :created, :updated, :deleted)";
                   $stmt=  $this->connection->prepare($qery);
                   $result=$stmt->execute(array(
                    ':id' => Null,
                    ':uni' => uniqid(),
                   ':labinfo_id' => $this->lab,
                    ':software_title' => $this->software_title,
                    ':version' => $this->version,
                   ':software_type' => $this->software_type,
                   ':created' => date("Y-m-d h:i:sA"),
                   ':updated' => date("Y-m-d h:i:sA"),
                   ':deleted' => date("Y-m-d h:i:sA"),
                   ':deleted' => date("Y-m-d h:i:sA"),
                       ));
                       if ($result)
                       {
                        $_SESSION['id']=$userid;
                         $_SESSION['error_msg']= '<b style="color: blue;">Software Inserted</b>';
                         unset($_SESSION['AllDAta']);
                         header('location:SoftAdd.php');
                       }
            } catch (Exception $ex) {
                
            }
        
        
    }
    
     public function loginuser()
       {
           try {
               $query= "SELECT * FROM users WHERE username="."'".$this->username."' "."AND password="."'".$this->password."'";
               $stmt=  $this->connection->prepare($query);
               $stmt->execute();
               $userdata=$stmt->fetch();
               echo "<pre>";
//               print_r($userdata);
               
               if($userdata)
               {
                   $_SESSION['username']=  $this->username;
                   $_SESSION['user'] = $userdata;
                  $_SESSION['userid'] = $userdata['id'];
//                  print_r($_SESSION['userid']);
//                  die();
                   $_SESSION['login_msg']='<b style=" color:blue; font-size: 16px; ">Login Successfull <br>Welcome this website';
               } 
               return $userdata;
           } catch (Exception $e) {
               $_SESSION['emty_msg']="request error....!!!";
                header('location:login.php');
           }
       }
       
    public function Soft_show()
    {
        $qry="SELECT * FROM installed_softwares WHERE unique_id="."'".$this->unique_id."'";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $row=$stmt->fetch();
       return $row;
    }
       public function ViewAllSoft()
    {
       $qry="SELECT * FROM installed_softwares";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }
    
        public function logout()
    {
            unset( $_SESSION['emty_msg']);
            unset($_SESSION['user'] );
            unset($_SESSION['login_msg']);
            header('location:login.php');
    }
    
//     $query=  "UPDATE `profiles` SET `users_id` = '100', `first_name` = 'khairull', `last_name` = 'islaml', 
//                   `personal_mobile` = '1784983611', `home_phone` = '1789456894', `address` = 'nothingl', `nationality` = 'bangladeshil',
//                    `Religion` = 'inlaml', `blode_group` = 'B+l', `gender` = 'Malel', `picture` = 'nothingl', 
//                    `dateofbirth` = '09/11/1994l', `nationalID` = 'nothingl', `lastEducationalStatus` = 'csel', `occupation` = 'Employeel', 
//                    `created` = '2:11:44 a', `modified` = '2:11:44 a', `deleted` = '2:11:44 a' WHERE `profiles`.`id` = 1; ";
    
    public function SoftUpdate()
    {   
        try {
//              UPDATE `users` SET `full_name` = 'khairul islam a', `email` = 'khairulislamtpi76@gmail.coma', `password` = 'khaiirula', `image` = 'a' WHERE `users`.`id` = 5;


              $qurey = "UPDATE `installed_softwares` SET `software_title` = '$this->software_title', `version` = '$this->version', `software_type` = '$this->software_type', `labinfo_id` = '$this->lab' WHERE `installed_softwares`.`unique_id`="."'".$this->unique_id."'";
//               echo $qurey;
//               die();
              $stmt = $this->connection->prepare($qurey);
                $result=$stmt->execute();
                if($result)
                {
                $_SESSION['update_msg']='<b style=" color: green;">Data Update Successful</b>';
                unset($_SESSION['AllDAta']);
                header("location:SoftList.php");
                }
        } catch (Exception $e) {
            
        }
       
    }
    
    
    public function SoftDelete()
    {
        try {
              $query = "DELETE FROM `installed_softwares` WHERE `installed_softwares`.`unique_id`="."'".$this->unique_id."'";
              
//              echo $query;
//              die();
              $stmt = $this->connection->prepare($query);
                if($stmt->execute())
                {
                    $_SESSION['update_msg']='<b style=" color: red;">Software Successfully Deleted</b>';
                    header("location:SoftList.php");
                }    
        } catch (Exception $ex) {
            
        }
    }

    

    public function userprofile()
    {
       $query="SELECT * FROM profiles WHERE `profiles`.`users_id` =".$_SESSION['userid'];
       $stmt=  $this->connection->query($query);
       $stmt->execute();
       $fulltable=$stmt->fetch();
       return $fulltable;
    }


}
