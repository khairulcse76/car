﻿<?php 
include_once '../vendor/autoload.php';
use labApps\Lab\Schedule\Schedule;

$scheduleObject = new Schedule(); 
$data = $scheduleObject->ViewAllSchedule();

//echo "<pre>"; print_r($data);die();
include '../inc/header.php';


?>

<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="SoftAdd.php">New Software</a></li>
                <li class="ic-grid-tables"><a href="SoftList.php"><span></span>View All Instalation Softwer</a></li>
            </ul>
 </div>
<style>
    input[type=button], input[type=submit], input[type=button] {
    height: 40px;
    margin-left: 30px;
}
input[type=button]:hover, input[type=submit]:hover, input[type=button]:hover {
    color: #ffffff;
    background-color: #666666;
}

.kkmenu ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
}

.kkmenu li {
    float: left;
}

.kkmenu li a {
    display: block;
    color: #333333;
    text-align: center;
    padding: 10px 12px;
}

.kkmenu li a:hover {
    background-color: #cccccc;
    
}
</style>
<?php
include '../inc/sidebar.php';
 
 
?>
 
        <div class="grid_10">
            <div class="box round first grid">
                <span class="kkmenu">
                
                    <ul>
                        <li><a href="ScheduleAdd.php">New Schedule</a></li>
                        <li><a class="active" href="Overview.php">Training Schedule Overview</a></li>
                    <li>
                         <?php if(isset( $_SESSION['update_msg'])) {  ?>
                             
                    <span style="margin-left: 200px;"> <?php echo  $_SESSION['update_msg']; unset( $_SESSION['update_msg']); ?></span> 
                             
                             <?php } ?>  
                    </li>
                  </ul>
                     
                
                </span>                
                <div class="block">        
                         <table class="data display datatable" id="example">
                            
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Course Name</th>
							<th>Batch Name</th>
                                                        <th>Trainer Name</th>
							<th>Lab No</th>
                                                        <th>Status</th>
                                                        <th colspan="">Action</th>
						</tr>
					</thead>
					<tbody>
                                            <?php 
                                            $id=1;
                                            foreach ($data as $row) {
                                            ?>
                                                                                          
                                            <tr class="odd gradeX">
							<td><?php echo $id++; ?></td>
							<td><?php echo $row['title'] ?></td>
                                                        <td><?php echo $row['batch_no'] ?></td>
                                                        <td><?php echo $row['lead_trainer'] ?></td>
                                                        <td><?php echo $row['lab_id'] ?></td>
                                                        <?php if($row['is_running']==0) { ?>
                                                        <td><a href="CourseOFF.php?unique_id=<?php echo $row['unique_id']; ?>">OFF</a></td>
                                                        <?php } else {   ?>
                                                        <td><a href="CourseOn.php?unique_id=<?php echo $row['unique_id']; ?>">ON</a></td>
                                                        <?php } ?>
							<td><a href="ScheduleView.php?unique_id=<?php echo $row['unique_id']; ?>">View</a> ||
                                                            <a href="ScheduleDelete.php?unique_id=<?php echo $row['unique_id']; ?>">Delete</a></td>
                                            </tr>
                                                
                                            <?php } ?>
						
						
					</tbody>
                         </table>
               </div>
            </div>
        </div>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>
<?php include '../inc/footer.php';?>

