﻿<?php 
session_start();
include '../inc/header.php'; 

?> 

<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="UserAdd.php">Add User</a></li>
                                <li class="ic-grid-tables"><a href="Userlist.php"><span></span>View All User</a></li>
                <li class="ic-charts"><a href="#"><span>Visit Website</span></a></li>
            </ul>
 </div>
<style>
            body{
                background: #DCDDDF;
            }
            *{
    margin: 0;
    padding: 0;
    outline: 0;
}
    .wrapper{
        width: 900px;
        margin: 0 auto;
    }
   
    .content{
        min-height: 600px;
        overflow: hidden;
        width: 100%;
    }
     h3{
        line-height: 50px;
        margin: 0 auto;
        margin-bottom: 20px;
        text-align: center;
        color: #666666;
        text-decoration:underline solid #000033;
    }
    .msg{
        color: #0000ff;
        margin: 0 auto;
        text-align: center;
        font-size: 16px;
        font-weight: bold;
        text-transform: capitalize;
    }
    .login_reg{
        background: #F9F9F9 none repeat scroll 0 0;
        margin: 0 auto;
        margin-top: 50px;
        border:1px solid #999999;
        width: 500px;
        padding: 20px;

    }
    input[type="text"],input[type="email"],input[type="password"]{
        border: 1px solid #d5d5d5;
        margin-bottom: 6px;
        margin-left: 10px;
        width: 285px;
        padding: 5px;
    }
    input[type="submit"],input[type="reset"],input[type="button"]{
    background: #888; 
    border: 1px solid #626262;
    border-radius: 3px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    padding: 5px 15px;
    float: right;
    margin-right: 20px;
    }
    input[type="submit"]:hover,input[type="reset"]:hover{
        background: #666;
    }
    table{
        margin: 0 auto;
    }
   
    .footer{
        background: #cccccc none repeat scroll 0 0;
        padding: 20px;
        color: #fff;
        text-align: center;
    }
        </style>
<?php 

include '../inc/sidebar.php';
 
 
?>

<div class="grid_10">
            <div class="box round first grid">
                <h2>New User
                
                 <?php 
                                if(isset($_SESSION['error_msg']))
                                { ?>
                                <span style="color: red; text-align:  center; font-size: 16px; margin-left: 200px;"><?php
                                echo $_SESSION['error_msg'];
                                unset($_SESSION['error_msg']);
                                ?></span>
                                <?php }

                                ?>
                </h2>
                
               <div class="block copyblock"> 
                   <form action="userstore.php" method="POST">
                        <table>
                            
                            <tr>
                                <td>Full Name<br>
                                    <input type="text" name="fullname" placeholder="Please give your Full Name" 
                                     value="<?php if(isset($_SESSION['AllDAta']['fullname'])){ echo $_SESSION['AllDAta']['fullname']; unset($_SESSION['AllDAta']['fullname']); } ?>"></td>
                            </tr>
                            <tr>
                                <td>User Name <span style="color:red">*</span><br>
                                    <input type="text" name="username" placeholder="Please give your username" 
                                           value="<?php if(isset($_SESSION['AllDAta']['username'])){ echo $_SESSION['AllDAta']['username']; unset($_SESSION['AllDAta']['username']); } ?>"></td>
                            </tr>
                            
                             <tr>
                                <td>Password <span style="color:red">*</span><br>
                                    <input type="password" name="password" placeholder="Please give your password" 
                                           value="<?php if(isset($_SESSION['AllDAta']['password'])){ echo $_SESSION['AllDAta']['password']; unset($_SESSION['AllDAta']['password']); } ?>"></td>
                            </tr>
                           
                             <tr>
                                <td>Confirm Password <span style="color:red">*</span><br>
                                    <input type="password" name="confmpass" placeholder="Reatype your password"
                                           value="<?php if(isset($_SESSION['AllDAta']['confmpass'])){ echo $_SESSION['AllDAta']['confmpass']; unset($_SESSION['AllDAta']['confmpass']); } ?>"></td>
                            </tr>
                            
                              <tr>
                                <td>Email<span style="color:red">*</span><br>
                                    <input type="text" name="email" placeholder="Please give your Email address" 
                                           value="<?php if(isset($_SESSION['AllDAta']['email'])){ echo $_SESSION['AllDAta']['email']; 
                                               unset($_SESSION['AllDAta']['email']); } ?>"><br/>
                                    
                                     <?php 
                                        if(isset($_SESSION['error_email'])) { ?>
                                        <p style="color: red; text-align:  center; font-size: 16px;">
                                            <?php  echo $_SESSION['error_email']; unset($_SESSION['error_email']); ?></p> <?php } ?>
                                    
                                </td>
                            </tr>
                             <tr>
                                 <td> Upload Profile Picture [picture must less then 300kb]<br>
                                    <input type="file" name="file"
                                           value="<?php if(isset($_SESSION['AllDAta']['file'])){ echo $_SESSION['AllDAta']['file']; unset($_SESSION['AllDAta']['file']); } ?>"></td>
                            </tr>
                             <tr>
                                 <td  colspan=""><br><input type="submit" value="Register">
                                     <input type="reset" value="Reset">
                                     <!--<input type="button" value="Login" onclick="window.location.href='./login.php'">-->
                                 </td>
                            </tr>

                        </table>
                    </form>
                   
                   
<!--                   <form action="userstore.php" method="POST">
                    <table class="form">
                        <tr>
                            <?php if(isset($_SESSION['error_msg'])) { echo $_SESSION['error_msg']; unset($_SESSION['error_msg']); } ?>
                        </tr>
                        <tr>
                            <td>Full Name <span style="color: red;">*</span> <br><input type="text" name="fullName" value="<?php if(isset($_SESSION['AllDAta']['fullName'])){ echo $_SESSION['AllDAta']['fullName']; unset($_SESSION['AllDAta']['fullName']); } ?>" placeholder="Enter Full Name..." class="medium" /> <?php if(isset($_SESSION['fulN_emt_msg'])) { echo $_SESSION['fulN_emt_msg']; unset($_SESSION['fulN_emt_msg']); } ?></td>
                        </tr>
                         <tr>
                            <td>User Name<span style="color: red;">*</span> <br><input type="text" name="userName" value="<?php if(isset($_SESSION['AllDAta']['userName'])){ echo $_SESSION['AllDAta']['userName']; unset($_SESSION['AllDAta']['userName']); } ?>" placeholder="Enter User Name..." class="medium" /><?php if(isset($_SESSION['u_emt_msg'])) { echo $_SESSION['u_emt_msg']; unset($_SESSION['u_emt_msg']); } ?></td>
                        </tr>
                         <tr>
                             <td>Password<span style="color: red;">*</span><br><input type="password" name="password" value="<?php if(isset($_SESSION['AllDAta']['password'])){ echo $_SESSION['AllDAta']['password']; unset($_SESSION['AllDAta']['password']); } ?>" placeholder="**************..." class="medium" /><?php if(isset($_SESSION['E_emt_msg'])) { echo $_SESSION['E_emt_msg']; unset($_SESSION['E_emt_msg']); } ?></td>
                        </tr>
                         <tr>
                             <td>E-mail<span style="color: red;">*</span> <br><input type="email" name="email" placeholder="khariul@gmail.com..." value="<?php if(isset($_SESSION['AllDAta']['email'])){ echo $_SESSION['AllDAta']['email']; unset($_SESSION['AllDAta']['email']); } ?>" class="medium" /><?php if(isset($_SESSION['p_emt_msg'])) { echo $_SESSION['p_emt_msg']; unset($_SESSION['p_emt_msg']); } ?></td>
                        </tr>
                         <tr>
                             <td>Profile Picture <span style="color: red;">*</span> <br><input type="file" name="image" value="<?php if(isset($_SESSION['AllDAta']['image'])){ echo $_SESSION['AllDAta']['image']; unset($_SESSION['AllDAta']['image']); } ?>" class="medium" /><?php if(isset($_SESSION['im_emt_msg'])) { echo $_SESSION['im_emt_msg']; unset($_SESSION['im_emt_msg']); } ?></td>
                        </tr>
			<tr> 
                            <td>
                                <input type="submit" Value="Insert" />
                            </td>
                        </tr>
                    </table>
                    </form>-->
                </div>
            </div>
        </div>

<?php include '../inc/footer.php';?>