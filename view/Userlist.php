﻿<?php 
include_once '../vendor/autoload.php';
use labApps\Lab\User\users;
$object=new users();

$data=$object->ViewAllUser();
include '../inc/header.php';


?>
<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="UserAdd.php">Add User</a></li>
                                <li class="ic-grid-tables"><a href="Userlist.php"><span></span>View All User</a></li>
                <li class="ic-charts"><a href="#"><span>Visit Website</span></a></li>
            </ul>
 </div>
<?php
include '../inc/sidebar.php';
 
 
?>
 
        <div class="grid_10">
            <div class="box round first grid">
                <h2>User List
                
                    
                      <?php if(isset( $_SESSION['update_msg'])) {  ?>
                             
                    <span style="margin-left: 200px;"> <?php echo  $_SESSION['update_msg']; unset( $_SESSION['update_msg']); ?></span> 
                             
                             <?php } ?>  
                
                </h2>
                 
                                         
                <div class="block">        
                         <table class="data display datatable" id="example">
                            
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Full Name</th>
							<th>User Name</th>
                                                        <th>email</th>
							<th>password</th>
							<th>image</th>
                                                        <th colspan="">Action</th>
						</tr>
					</thead>
					<tbody>
                                            <?php 
                                            $id=1;
                                            foreach ($data as $row) {
                                            ?>
                                                                                          
                                            <tr class="odd gradeX">
							<td><?php echo $id++; ?></td>
							<td><?php echo $row['full_name'] ?></td>
                                                        <td><?php echo $row['username'] ?></td>
                                                        <td><?php echo $row['email'] ?></td>
                                                        <td><?php echo $row['password'] ?></td>
                                                        <td><?php echo $row['image'] ?></td>
							<td><a href="UserEdit.php?unique_id=<?php echo $row['unique_id']; ?>">Edit</a> ||
                                                            <a href="UserDelete.php?unique_id=<?php echo $row['unique_id']; ?>">Delete</a></td>
                                            </tr>
                                                
                                            <?php } ?>
						
						
					</tbody>
                         </table>
               </div>
            </div>
        </div>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>
<?php include '../inc/footer.php';?>

