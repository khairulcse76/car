﻿<?php 
    session_start();
    include 'inc/header.php';
    include 'inc/sidebar.php';
    include_once '../vendor/autoload.php';
    use ecommerce\catagory;
    
    $catagoryobj=new catagory();
     $data=$catagoryobj->prepare($_GET)->show();
    
   
        
    ?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Add New Category</h2>
               <div class="block copyblock"> 
                   <form action="catupdate.php" method="POST">
                    <table class="form">
                      <?php if(isset($_SESSION['store_msg'])){ ?>
                        <tr>
                            <td><?php echo $_SESSION['store_msg']; unset($_SESSION['store_msg']); ?></td>
                        </tr>
                      <?php } ?>
                        <tr>
                            <td>
                                <input type="text" name="catname" value="<?php  if(isset($data['catName'])) {
                                echo $data['catName']; } ?>" class="medium" />
                            </td>
                             <td>
                                 <input type="hidden" name="unique_id" value="<?php  if(isset($data['unique_id']))
                                { echo $data['unique_id']; }?>" class="medium" />
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <input type="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
<?php include 'inc/footer.php';
    
?>