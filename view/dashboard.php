﻿<?php
include_once '../vendor/autoload.php';
use labApps\Lab\Dashboard\dashboard;

$dashboardOBJ = new dashboard();
$datadash=$dashboardOBJ->ViewAllSchedule();

if(isset($_SESSION['user']))
{
//    header( "refresh:2"); 
include '../inc/header.php';

?>
<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='https://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <style type="text/css">
            #login{
                /*width: 95%;*/
                padding: 5px 0px 5px 0px;
                border: 1px solid #64b0ce;
                box-shadow: 0pt 2px 5px #3ebff3, 0px 0px 8px 5px #41b1de;
                -moz-box-shadow: 0pt 2px 5px #3ebff3, 0px 0px 8px 5px #41b1de;
                border-radius: 5px;
                background: #fff;
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #fff), color-stop(1, #fff));
                background: -moz-gradient(linear, left top, left bottom, color-stop(0.05, fff), color-stop(1, fff));
                background: -o-gradient(linear, left top, left bottom, color-stop(0.05, #fff), color-stop(1, #fff));
                /*margin: 20px 0px 0px 2%;*/
                text-align: center;
            }
            .btn-info {
                color: #fff;
                background-color: #5bc0de;
                border-color: #46b8da;
            }
            .kmain{
                width: 1080px; 
                height: 400px;
                margin-top: 10px;
            }
            .mainleft{
               width: 380px; height: 300px; float: left;
            }
            .mainright{
                width: 380px; height: 300px; float: left; margin-left: 40px;
            }
            .mainrightmenu{
                height: 400px; float: left; margin-left: 40px;  background-image: url("../image/side.png"); width: 200px; margin-top: 10px;
            }
            .running_course{
                height: 190px; float: left; background-image: url("../image/side.png"); width: 380px; border: 10px solid #ffffff;
             }
             .lastLogin_time{
               height: 190px; float: left; background-image: url("../image/side.png"); width: 380px; border: 10px solid #ffffff;
             }.Upcoming_course{
              height: 190px; float: left; background-image: url("../image/side.png"); width: 380px; border: 10px solid #ffffff;
             }
             .recentcomplete{
                 height: 190px; float: left; background-image: url("../image/side.png"); width: 380px; border: 10px solid #ffffff;
             }
             
        </style>
    </head>
    
 <div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-charts"><a href="Overview.php"><span>Training Schedule</span></a></li>
                <li class="ic-form-style"><a href="Userlist.php"><span>User Info</span></a></li>
                <li class="ic-typography"><a href="TrainerList.php">Trainer Information</a></li>
                <li class="ic-grid-tables"><a href=""><span></span>Course Info</a></li>
                <li class="ic-charts"><a href="SoftList.php"><span>Software Information</span></a></li>
            </ul>
 </div>
<?php include '../inc/sidebar.php';?>


<div class="grid_10">
   
    <div class="box round first grid">
        
      
      <div id="article">
                <div id="login" class="animate form">
                     <?php if(isset($_SESSION['login_msg'])) { ?>
                    <span style="color:green; font-size: 10px;"><?php echo $_SESSION['login_msg']; unset($_SESSION['login_msg']); ?></span><br>
              <?php } ?>
                    <span style="font-size: 20px; font-weight: bold; color: #2E5E79; text-align: center;">Welcome to Backs\ash Dashboard</span>
               
                
                </div>
          
          <div class="kmain">
              <div class="mainleft">
                  
                  <div class="running_course">
                      <table style="margin: 0 auto;" border="2">
                          <tr>
                              <td style=" width: 380px; text-align: center " colspan="4"><h2>Running Five course</h2></td>
                          </tr>
                          <tr>
                              <th>SL</th>
                              <th> Course Name</th>
                              <th> Start Time</th>
                              <th> Ending Time</th
                          </tr>
                          <?php
                          $id=1;
                       foreach ($datadash as $data){ ?>
                           <tr>
                               <td style=" color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 3px; "><?php echo $id++ ?></td>
                              <td style="color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 3px; "><?php echo $data['title'] ?></td>
                              <td style="color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 3px; "><?php echo $data['start_time'] ?></td>
                              <td style="color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 3px; "><?php echo $data['ending_time'] ?></td>
                          </tr>
                      <?php }
                          ?>
                          
                      </table>
                  </div>
                  <div class="lastLogin_time">
                    <table style="margin: 0 auto;" border="2">
                          <tr>
                              <td style=" width: 380px; text-align: center " colspan="4"><h2>Last Login Time</h2></td>
                          </tr>
                          <tr>
                              <th>SL</th>
                              <th> User Name</th>
                              <th> Login Time</th>
                              <th> Logout Time</th>
                          </tr>
                          <?php
                          $logintime=$dashboardOBJ->LoginTime();
                          $id=1;
                       ?>
                           <tr>
                               <td style=" color: #ffffff; text-align: center; border-bottom: 0px solid #666666; padding: 5px; "><?php echo $id++ ?></td>
                              <td style="color: #ffffff; text-align: center; border-bottom: 0px solid #666666; padding: 5px; "><?php echo $logintime['username'] ?></td>
                              <td style="color: #ffffff; text-align: center; border-bottom: 0px solid #666666; padding: 5px; "><?php echo $logintime['Logintime'] ?></td>
                              <td style="color: #ffffff; text-align: center; border-bottom: 0px solid #666666; padding: 5px; "><?php echo $logintime['Logouttime'] ?></td>
                          </tr>
                     
                          
                      </table>
                  </div>
                  
              </div>
              <div class="mainright">
                  
                  <div class="Upcoming_course">
                   <table style="margin: 0 auto;" border="2">
                          <tr>
                              <td style=" width: 380px; text-align: center " colspan="4"><h2>Upcoming Course</h2></td>
                          </tr>
                          <tr>
                              <th>SL</th>
                              <th> Course Name</th>
                          </tr>
                          <?php
                          $id=1;
                          $upcomingCourses=$dashboardOBJ->UpcomingCourse();
                       foreach ($upcomingCourses as $updata){ ?>
                           <tr>
                               <td style=" color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 5px; "><?php echo $id++ ?></td>
                              <td style="color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 5px; "><?php echo $updata['title'] ?></td>
                             
                          </tr>
                      <?php }
                          ?>
                          
                      </table>
                  </div>
                  <div class="recentcomplete">
                   <table style="margin: 0 auto;" border="2">
                          <tr>
                              <td style=" width: 380px; text-align: center " colspan="4"><h2>Contact With Lead Trainer</h2></td>
                          </tr>
                          <tr>
                              <th>SL</th>
                              <th> Trainer Name</th>
                              <th>E-mail</th>
                              <th>Phone Number</th>
                          </tr>
                          <?php
                          
                          $id=1;
                          $TRAINERVIEW=$dashboardOBJ->TrainerView();
                       foreach ($TRAINERVIEW as $TRAINER){ ?>
                           <tr>
                               <td style=" color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 5px; "><?php echo $id++ ?></td>
                              <td style="color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 5px; "><?php echo $TRAINER['full_name'] ?></td>
                              <td style="color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 5px; "><?php echo $TRAINER['email'] ?></td>
                              <td style="color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 5px; "><?php echo $TRAINER['phone'] ?></td>
                          </tr>
                      <?php }
                          ?>
                          
                      </table>
                  </div>
              </div>
              <div class="mainrightmenu">
                  <h2 style="width: 100%; text-align: center; ">Recent Completed Course</h2>
                  <table style="width: 100%;">
                      <tr>
                          <th>SL</th>
                          <th>Name</th>
                      </tr>
                      <?php
                          $id=1;
                          $completedcourse=$dashboardOBJ->ResentCompletedCourse();
                       foreach ($completedcourse as $complete){ ?>
                      <tr>
                           <td style=" color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 5px; "><?php echo $id++ ?></td>
                              <td style="color: #ffffff; text-align: center; border-top: 1px solid #666666; padding: 5px; "><?php echo $complete['title'] ?></td>
                      </tr>
                       <?php } ?>
                  </table>
              </div>
              
          </div>

               
            
            </div>
    </div>
</div>
<?php include '../inc/footer.php';


}  else {
            $_SESSION['err_msg']='<b style=" color:blue; font-size: 16px; ">You must Login.';
           header('location:login.php');
}
?>