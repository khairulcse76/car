<?php
include_once '../vendor/autoload.php';
use labApps\Lab\Trainers\Trainers;
$trainerObject=new Trainers();


$_SESSION['AllDAta']=$_POST;

$firstName=$_POST['firstName'];
$lastDegree=$_POST['lastDegree'];
$University=$_POST['University'];
$passingYear=$_POST['passingYear'];
$team=$_POST['team'];
$trainerstatus=$_POST['trainerstatus'];
$phone=$_POST['phone'];
$web=$_POST['web'];
$gender=$_POST['gender'];

if(empty($firstName) || empty($lastDegree) || empty($passingYear) || empty($team) || empty($trainerstatus) 
        || empty($phone) || empty($gender) ){
    
    $_SESSION['error_msg']='<Span style="color:red;">(*) Field is Required...</Span>';
    header('location:TrainerAdd.php');
} else {
        $web = test_input($_POST["web"]);
//     check if URL address syntax is valid
        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$web)) {
        $_SESSION['error_msg'] = "Invalid URL..."; 
        header('location:TrainerAdd.php');
        $trainerObject->prepare($_POST);
        
        }
       
    }   
    
    
//   echo "<pre>";
//print_r($_POST);
////die();
//  }




function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}