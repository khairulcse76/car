﻿<?php 
include_once '../vendor/autoload.php';
use labApps\Lab\Trainers\Trainers;
use labApps\Lab\User\users;

$trainerObject=new Trainers();



$data=$trainerObject->ViewAllTrainer();
include '../inc/header.php';


?>
<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="TrainerAdd.php">Add Trainer info</a></li>
                <li class="ic-grid-tables"><a href="TrainerList.php"><span></span>Trainer List</a></li>
                <li class="ic-charts"><a href="#"><span>Visit Website</span></a></li>
            </ul>
 </div>
<?php
include '../inc/sidebar.php';
 
 
?>
 
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Trainer Information
                
                    
                      <?php if(isset( $_SESSION['update_msg'])) {  ?>
                             
                    <span style="margin-left: 200px;"> <?php echo  $_SESSION['update_msg']; unset( $_SESSION['update_msg']); ?></span> 
                             
                             <?php } ?>  
                
                </h2>
                 
                                         
                <div class="block">        
                         <table class="data display datatable" id="example">
                            
					<thead>
						<tr>
							<th>SN.</th>
							<td>Full Name</td>
							<td>Education Status</td>
                                                        <th>Team</th>
							<th>Capable Course</th>
							<th>Trainer Level</th>
                                                        <th>Image</th>
							<th>Phone</th>
							<th>Email</th>
                                                        <th>Address</th>
							<th>Gender</th>
                                                        <th colspan="">Action</th>
						</tr>
					</thead>
					<tbody>
                                            <?php 
                                            $id=1;
                                            foreach ($data as $row) {
                                            ?>
                                                                                          
                                            <tr class="odd gradeX">
							<td><?php echo $id++; ?></td>
							<td><?php echo $row['full_name'] ?></td>
                                                        <td><?php echo $row['edu_status'] ?></td>
                                                        <td><?php echo $row['team'] ?></td>
                                                        <td><?php echo $row['courses_id'] ?></td>
                                                        <td><?php echo $row['trainer_status'] ?></td>
                                                        
                                                        
							<td><?php echo $row['image'] ?></td>
                                                        <td><?php echo $row['phone'] ?></td>
                                                        <td><?php echo $row['email'] ?></td>
                                                        <td><?php echo $row['address'] ?></td>
                                                        <td><?php echo $row['gender'] ?></td>
<!--                                                        
							<td><?php // echo $row['web'] ?></td>-->
							<td><a href="TrainerEdit.php?unique_id=<?php echo $row['unique_id']; ?>">Edit</a> ||
                                                            <a href="UserDelete.php?unique_id=<?php echo $row['unique_id']; ?>">Delete</a></td>
                                            </tr>
                                                
                                            <?php } ?>
						
						
					</tbody>
                         </table>
               </div>
            </div>
        </div>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>
<?php include '../inc/footer.php';?>

