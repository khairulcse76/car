﻿<?php 
include_once '../vendor/autoload.php';
use labApps\Lab\User\users;
$object=new users();

$data=$object->prepare($_GET)->User_show();


include '../inc/header.php';
?> 

<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="UserAdd.php">Add User</a></li>
                <li class="ic-grid-tables"><a href="Userlist.php"><span></span>View All User</a></li>
                <li class="ic-charts"><a href="#"><span>Visit Website</span></a></li>
            </ul>
 </div>
<?php 
include '../inc/sidebar.php';
 
 
?>

<div class="grid_10">
            <div class="box round first grid">
                <h2>User Update</h2>
               <div class="block copyblock"> 
                   <form action="UserUpdate.php" method="POST" enctype="">
                    <table class="form">
                        <tr>
                            <?php if(isset($_SESSION['error_msg'])) { echo $_SESSION['error_msg']; unset($_SESSION['error_msg']); } ?>
                        </tr>
                        <tr>
                            <td>Full Name <span style="color: red;">*</span> <br><input type="text" name="fullName" value="<?php if(isset($data['full_name'])){ echo $data['full_name']; unset($data['full_name']); } if(isset($_SESSION['AllDAta']['fullName'])){ echo $_SESSION['AllDAta']['fullName']; unset($_SESSION['AllDAta']['fullName']); } ?>"/> </td>
                        </tr>
                         <tr>
                            <td>User Name<span style="color: red;">*</span> <br><input type="text" name="userName" value="<?php if(isset($data['username'])){ echo $data['username']; unset($data['username']); } if(isset($_SESSION['AllDAta']['userName'])){ echo $_SESSION['AllDAta']['userName']; unset($_SESSION['AllDAta']['userName']); } ?>"/></td>
                        </tr>
                         <tr>
                             <td>Password<span style="color: red;">*</span><br><input type="password" name="password" value="<?php if(isset($data['password'])){ echo $data['password']; unset($data['password']); }
                             if(isset($_SESSION['AllDAta']['password'])){ echo $_SESSION['AllDAta']['password']; unset($_SESSION['AllDAta']['password']); }  ?>"/></td>
                        </tr>
                         <tr>
                             <td>E-mail<span style="color: red;">*</span> <br><input type="email" name="email" value="<?php if(isset($data['email'])){ echo $data['email']; unset($data['email']); }
                             if(isset($_SESSION['AllDAta']['email'])){ echo $_SESSION['AllDAta']['email']; unset($_SESSION['AllDAta']['email']); }  ?>"/></td>
                        </tr>
                         <tr>
                             <td>Profile Picture <span style="color: red;">*</span> <br><input type="file" name="image" value="<?php if(isset($data['image'])){ echo $data['image']; unset($data['image']); }
                             if(isset($_SESSION['AllDAta']['image'])){ echo $_SESSION['AllDAta']['image']; unset($_SESSION['AllDAta']['image']); }  ?>"/></td>
                        </tr>
                         <tr>
                             <td><span style="color: red;"></span> <br><input type="hidden" name="unique_id" value="<?php if(isset($data['unique_id'])){ echo $data['unique_id']; unset($data['unique_id']); }
                             if(isset($_SESSION['AllDAta']['unique_id'])){ echo $_SESSION['AllDAta']['unique_id']; unset($_SESSION['AllDAta']['unique_id']); } ?>"/></td>
                        </tr>
			<tr> 
                            <td>
                                <input type="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>

<?php include '../inc/footer.php';?>