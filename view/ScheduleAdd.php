<?php 
session_start();
include_once '../vendor/autoload.php';
use labApps\Lab\Schedule\Schedule;
use labApps\Lab\Software\Software;
use labApps\Lab\LabInfo\LabInfo;
use labApps\Lab\Course\Course;
use labApps\Lab\Trainers\Trainers;
use labApps\Lab\User\users;



include '../inc/header.php';
?> 
<style>
table, th {
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}
input[type=button], input[type=submit] {
    background-color: #666666;
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 2px;
    cursor: pointer;
    float: right;
}
input[type=button],input[type=reset] {
    background-color: #666666;
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 2px;
    cursor: pointer;
    float: left;
}
</style>
<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="ScheduleAdd.php">New Training Schedule</a></li>
                <li class="ic-grid-tables"><a href="Overview.php"><span></span>Overview Training Schedule</a></li>
                <li class="ic-charts"><a href="http://www.bitm.org.bd/"><span>Visit Website</span></a></li>
            </ul>
 </div>
<?php 
include '../inc/sidebar.php';
 
 
?>
 <div class="grid_10">
            <div class="box round first grid">
                <h2>Training Management
                
                    
                      <?php if(isset( $_SESSION['error_msg'])) {  ?>
                             
                    <span style="margin-left: 200px;"> <?php echo  $_SESSION['error_msg']; unset( $_SESSION['error_msg']); ?></span> 
                             
                             <?php } ?>  
                             <span style="float: right; ">
                                  Assign by
                                    <select name="Assignby">
                                            <option><?php if(isset($_SESSION['full_name']))
                                                {
                                                    echo $_SESSION['full_name'];
                                                }?>
                                            </option>
                                        </select>
                                    </td>
                               </span>
                
                </h2>
                <form action="ScheduleStore.php" method="Post">

                    <table>
                      <tr>
                        <td><h2>Course Info</h2>
                            <table>
                                <tr>
                                    <td>Course Name <span style="color:red">*</span><br>
                                        <select style="width: 100%;" name="courseId" onchange="getId(this.value)">
                                            <?php if(isset($_SESSION['AllDAta']['courseId'])){ ?>
                                                <option><?php echo $_SESSION['AllDAta']['courseId']; unset($_SESSION['AllDAta']['courseId']); ?></option> 
                                              <?php } ?>
                                            <option>Select Course Name</option>
                                           <?php 
                                                $courseObj= new Course();
                                               $data= $courseObj->ViewAllcourses();
                                                foreach ($data as $value)
                                                { ?>
                                            <option value="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></option>
                                               <?php }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Lab Name <span style="color:red">*</span><br>
                                        <select style="width: 100%;" name="labid">
                                            <?php if(isset($_SESSION['AllDAta']['labid'])){ ?>
                                                <option><?php echo $_SESSION['AllDAta']['labid']; unset($_SESSION['AllDAta']['labid']); ?></option> 
                                              <?php } ?>
                                            <option>Lab Name</option>
                                            <?php 
                                                $LabObj= new LabInfo();
                                               $data= $LabObj->ViewAlllabinfo();
                                                foreach ($data as $value)
                                                { ?>
                                                    <option><?php echo $value['lab_no'] ?></option>
                                               <?php }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Batch No <span style="color:red">*</span><br>
                                        <input type="text" name="BatchNo" style="width: 100%; height: 20px;" placeholder="Enter Batch"
                                               value="<?php if(isset($_SESSION['AllDAta']['BatchNo'])){ echo $_SESSION['AllDAta']['BatchNo']; 
                                               unset($_SESSION['AllDAta']['BatchNo']); } ?>"/>
                                    </td>
                                </tr>
                            </table>
                            
                        <input type="reset" value="Reset"/>
                        <input type="submit" value="Submit"/>
                        </td>
                        <td><h2>Trainer</h2> <table>
                                <tr>
                                    <td>Lead Trainer <span style="color:red">*</span><br>
                                        <select style="width: 100%;" name="trainerId">
                                            <?php if(isset($_SESSION['AllDAta']['trainerId'])){ ?>
                                                <option><?php echo $_SESSION['AllDAta']['trainerId']; unset($_SESSION['AllDAta']['trainerId']); ?></option> 
                                              <?php }else { ?>
                                            <option>Select Lead Trainer</option>
                                              <?php }
                                                $trainerObj= new Trainers();
                                               $trainerdata= $trainerObj->ViewLeadTrainer();
                                                foreach ($trainerdata as $value)
                                                { ?>
                                                    <option><?php echo $value['full_name'] ?></option>
                                               <?php }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Assistant Trainer <span style="color:red"></span><br>
                                        <select style="width: 100%;" name="assTrainer" id="assTrainer">
                                             <?php if(isset($_SESSION['AllDAta']['assTrainer'])){ ?>
                                                <option><?php echo $_SESSION['AllDAta']['assTrainer']; unset($_SESSION['AllDAta']['assTrainer']); ?></option> 
                                              <?php } ?>
                                            <option>Select Assistant Trainer</option>
                                            
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Lab Assistant <span style="color:red"></span><br>
                                       <select style="width: 100%;" name="labassistant">
                                            <?php if(isset($_SESSION['AllDAta']['labassistant'])){ ?>
                                                <option><?php echo $_SESSION['AllDAta']['labassistant']; unset($_SESSION['AllDAta']['labassistant']); ?></option> 
                                              <?php } ?>
                                            <option>Select Lab Assistant</option>
                                           <?php 
                                                $trainer=new Trainers();
                                                $assistantTrainer=$trainer->viewAllAssistaint();
                                                foreach ($assistantTrainer as $asValue)
                                                { ?>
                                            <option><?php echo $asValue['full_name'] ?></option>
                                               <?php }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                               
                            </table>
                        </td>
                        <td> <h2>Duration</h2>
                            <table>
                                <tr>
                                    <td><h2>Start Date</h2><br><label>Start Date<span style="color:red">*</span></label>
                                        <input type="date" name="strtDate" value="
                                               <?php if(isset($_SESSION['AllDAta']['strtDate'])){ echo $_SESSION['AllDAta']['strtDate']; 
                                               unset($_SESSION['AllDAta']['strtDate']); } ?>
                                               " placeholder="m/d/y"/>
                                        <label>End Date<span style="color:red">*</span>
                                        </label><input type="date" name="endDate" value="
                                              <?php if(isset($_SESSION['AllDAta']['endDate'])){ echo $_SESSION['AllDAta']['endDate']; 
                                               unset($_SESSION['AllDAta']['endDate']); } ?>
                                              "/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h2>Days Per Week
                                        <select name="days" style="width: 100%; height: 40px;">
                                            <?php if(isset($_SESSION['AllDAta']['days'])){ ?>
                                              <option><?php echo $_SESSION['AllDAta']['days']; unset($_SESSION['AllDAta']['days']); ?></option> 
                                                 <?php } ?>
                                                <option>Select Days</option>
                                                <option>Saturday Monday Wednesday</option>
                                                 <option>Sunday Tuesday Thursday</option>
                                                  <option>Friday</option>
                                        </select>
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td><label>[Start Time]<span style="color:red">*</span></label><br>
                                               H:
                                                    <select name="strtH">
                                                        <?php if(isset($_SESSION['AllDAta']['strtH'])){ ?>
                                              <option><?php echo $_SESSION['AllDAta']['strtH']; unset($_SESSION['AllDAta']['strtH']); ?></option> 
                                                 <?php } ?>
                                              <option value="0">Hour</option>
                                                         <option>12</option>
                                                         <option>11</option>
                                                         <option>10</option>
                                                         <option>9</option>
                                                          <option>8</option>
                                                          <option>7</option>
                                                          <option>6</option>
                                                          <option>5</option>
                                                          <option>3</option>
                                                          <option>2</option>
                                                          <option>1</option>
                                                    </select>
                                                    <label>Munnite</label>
                                                    <select name="strtM">
                                                        <?php if(isset($_SESSION['AllDAta']['strtM'])){ ?>
                                              <option><?php echo $_SESSION['AllDAta']['strtM']; unset($_SESSION['AllDAta']['strtM']); ?></option> 
                                                 <?php } ?>
                                              <option value="0">Minute</option>
                                                             <option>00</option>
                                                             <option>10</option>
                                                             <option>15</option>
                                                             <option>30</option>
                                                              <option>40</option>
                                                              <option>45</option>
                                                              <option>50</option>
                                                              <option>59</option>
                                                        </select>
                                                </td>
                                                
                                                <td><label>[End Time]<span style="color:red">*</span></label><br>
                                                 H:
                                                    <select name="endH">
                                                         <?php if(isset($_SESSION['AllDAta']['endH'])){ ?>
                                                            <option><?php echo $_SESSION['AllDAta']['endH']; unset($_SESSION['AllDAta']['endH']); ?></option> 
                                                         <?php } ?>
                                                            <option value="0">Hour</option>
                                                         <option>12</option>
                                                         <option>11</option>
                                                         <option>10</option>
                                                         <option>9</option>
                                                          <option>8</option>
                                                          <option>7</option>
                                                          <option>6</option>
                                                          <option>5</option>
                                                          <option>3</option>
                                                          <option>2</option>
                                                          <option>1</option>
                                                    </select>
                                                    Munite
                                                    <select name="endM">
                                                         <?php if(isset($_SESSION['AllDAta']['endM'])){ ?>
                                                            <option><?php echo $_SESSION['AllDAta']['endM']; unset($_SESSION['AllDAta']['endM']); ?></option> 
                                                         <?php } ?>
                                                            <option value="0">Minute</option>
                                                             <option>00</option>
                                                             <option>10</option>
                                                             <option>15</option>
                                                             <option>30</option>
                                                              <option>40</option>
                                                              <option>45</option>
                                                              <option>50</option>
                                                              <option>59</option>
                                                        </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table></td>
                     <!--??/--<td colspan="3"><input type="submit" value="Submit"/><input type="reset" value="Reset"/></td>-->
                      </tr>
                      
                    </table>

                </form>
                <!--<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>-->
      </div>
 </div>
<?php include '../inc/footer.php';?>
        <script>
                    function getId(val){
                    $.ajax({
                          type: "post",
                          url: "TrainerAjax.php",
                          data: "courses_id="+val,
                          success: function (data){
                              $("#assTrainer").html(data);
                          }
                           
                          });
                    }
               </script>