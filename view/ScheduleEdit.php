<?php 
include_once '../vendor/autoload.php';
use labApps\Lab\Schedule\Schedule;
use labApps\Lab\Software\Software;
use labApps\Lab\LabInfo\LabInfo;
use labApps\Lab\Course\Course;
use labApps\Lab\Trainers\Trainers;
use labApps\Lab\User\users;

$editScheduleObj=new Schedule();
$rowdata=$editScheduleObj->prepare($_GET)->ScheduleEditPageshow();
//print_r($data);
include '../inc/header.php';
?> 
<style>
table, th {
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}
input[type=button], input[type=submit], input[type=button] {
    width: 100px;
    height: 40px;
    background-color: #cccccc;
}
input[type=button]:hover, input[type=submit]:hover, input[type=button]:hover {
    color: #ffffff;
    background-color: #666666;
}
</style>
<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="ScheduleAdd.php">New Training Schedule</a></li>
                <li class="ic-grid-tables"><a href="Overview.php"><span></span>Overview Training Schedule</a></li>
                <li class="ic-charts"><a href="http://www.bitm.org.bd/"><span>Visit Website</span></a></li>
            </ul>
 </div>
<?php 
include '../inc/sidebar.php';
 
 
?>
 <div class="grid_10">
            <div class="box round first grid">
                <h2>Training Management
                
                    
                      <?php if(isset( $_SESSION['error_msg'])) {  ?>
                             
                    <span style="margin-left: 200px;"> <?php echo  $_SESSION['error_msg']; unset( $_SESSION['error_msg']); ?></span> 
                             
                             <?php } ?>  
                             <span style="float: right; ">
                                  Assign by
                                    <select name="Assignby">
                                            <option><?php if(isset($_SESSION['full_name']))
                                                {
                                                    echo $_SESSION['full_name'];
                                                }?>
                                            </option>
                                        </select>
                                    </td>
                               </span>
                
                </h2>
                <form action="ScheduleUpdate.php" method="Post">

                    <table>
                      <tr>
                        <td><h2>Course Info</h2>
                            <table>
                                <tr>
                                    <td>Course Name <span style="color:red">*</span><br>
                                        <select style="width: 100%;" name="courseId" onchange="getId(this.value)">
                                            <?php if(isset($_SESSION['AllDAta']['courseId'])){ ?>
                                                <option><?php echo $_SESSION['AllDAta']['courseId']; unset($_SESSION['AllDAta']['courseId']); ?></option> 
                                              <?php } ?>
                                            <option>Select Course Name</option>
                                           <?php 
                                                $courseObj= new Course();
                                               $data= $courseObj->ViewAllcourses();
                                                foreach ($data as $value)
                                                { ?>
                                            <option 
                                                <?php if($rowdata['course_id']==$value['id']){ ?>
                                                selected="Selected"
                                              <?php  } ?>
                                                value="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></option>
                                               <?php }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Lab Name <span style="color:red">*</span><br>
                                        <select style="width: 100%;" name="labid">
                                            <?php if(isset($_SESSION['AllDAta']['labid'])){ ?>
                                                <option><?php echo $_SESSION['AllDAta']['labid']; unset($_SESSION['AllDAta']['labid']); ?></option> 
                                              <?php } ?>
                                            <option>Lab Name</option>
                                            <?php 
                                                $LabObj= new LabInfo();
                                               $data= $LabObj->ViewAlllabinfo();
                                                foreach ($data as $value)
                                                { ?>
                                                    <option <?php if($rowdata['lab_id']==$value['lab_no']){ ?>
                                                selected="Selected"
                                              <?php  } ?> ><?php echo $value['lab_no'] ?></option>
                                               <?php }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                
                                    <td>Batch No <span style="color:red">*</span><br>
                                        <input type="text" name="BatchNo" style="width: 100%; height: 20px;" placeholder="Enter Batch"
                                               value="<?php echo $rowdata['batch_no'] ?><?php if(isset($_SESSION['AllDAta']['BatchNo'])){ echo $_SESSION['AllDAta']['BatchNo']; 
                                               unset($_SESSION['AllDAta']['BatchNo']); } ?>"/>
                                    </td>
                                </tr>
                            </table>
                            
                        <input type="button" value="Back" onclick="window.location.href='./ScheduleView.php'"/>
                        <input type="submit" value="Update" />
                        </td>
                        <td><h2>Trainer</h2> <table>
                                <tr>
                                    <td>Lead Trainer <span style="color:red">*</span><br>
                                        <select style="width: 100%;" name="trainerId">
                                            <?php if(isset($_SESSION['AllDAta']['trainerId'])){ ?>
                                                <option><?php echo $_SESSION['AllDAta']['trainerId']; unset($_SESSION['AllDAta']['trainerId']); ?></option> 
                                              <?php }else { ?>
                                            <option>Select Lead Trainer</option>
                                              <?php }
                                                $trainerObj= new Trainers();
                                               $trainerdata= $trainerObj->ViewLeadTrainer();
                                                foreach ($trainerdata as $value)
                                                { ?>
                                                    <option <?php if($rowdata['lead_trainer']==$value['full_name']){ ?>
                                                selected="Selected"
                                              <?php  } ?>><?php echo $value['full_name'] ?></option>
                                               <?php }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Assistant Trainer <span style="color:red"></span><br>
                                        <select style="width: 100%;" name="assTrainer" id="assTrainer">
                                             <?php if(isset($_SESSION['AllDAta']['assTrainer'])){ ?>
                                                <option><?php echo $_SESSION['AllDAta']['assTrainer']; unset($_SESSION['AllDAta']['assTrainer']); ?></option> 
                                              <?php } ?>
                                            <option>Select Assistant Trainer</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Lab Assistant <span style="color:red"></span><br>
                                        <input type="text" name="labassistant" style="width: 100%; height: 20px;" placeholder="Enter Lab Assistant Name" 
                                               value="<?php echo $rowdata['lab_asst'] ?><?php if(isset($_SESSION['AllDAta']['labassistant'])){ echo $_SESSION['AllDAta']['labassistant']; 
                                               unset($_SESSION['AllDAta']['labassistant']); } ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="hidden" name="unique_id" value="<?php echo $rowdata['unique_id'] ?>
                <?php if(isset($_SESSION['AllDAta']['unique_id'])){ echo $_SESSION['AllDAta']['unique_id']; unset($_SESSION['AllDAta']['unique_id']); } ?>
                                               "</td>
                                </tr>
                               
                            </table>
                        </td>
                        <td> <h2>Duration</h2>
                            <table>
                                <tr>
                                    <td><h2>Start Date</h2><br><label>Start Date<span style="color:red">*</span></label>
                                        <input type="date" name="strtDate" value=" <?php echo $rowdata['start_date'];?>
                                               <?php if(isset($_SESSION['AllDAta']['strtDate'])){ echo $_SESSION['AllDAta']['strtDate']; 
                                               unset($_SESSION['AllDAta']['strtDate']); } ?>
                                               " placeholder="m/d/y"/>
                                        <label>End Date<span style="color:red">*</span>
                                        </label><input type="date" name="endDate" value=" <?php echo $rowdata['ending_date'];?>
                                              <?php if(isset($_SESSION['AllDAta']['endDate'])){ echo $_SESSION['AllDAta']['endDate']; 
                                               unset($_SESSION['AllDAta']['endDate']); } ?>
                                              "/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h2>Days Per Week
                                        <select name="days" style="width: 100%; height: 40px;">
                                            <?php if(isset($_SESSION['AllDAta']['days'])){ ?>
                                              <option><?php echo $_SESSION['AllDAta']['days']; unset($_SESSION['AllDAta']['days']); ?></option> 
                                                 <?php } ?>
                                                <option><?php echo $rowdata['day'];?></option>
                                                <option>Saturday Monday Wednesday</option>
                                                 <option>Sunday Tuesday Thursday</option>
                                                  <option>Friday</option>
                                        </select>
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td><label>[Start Time]<span style="color:red">*</span></label><br>
                                               H:
                                                    <select name="strtH">
                                                        <?php if(isset($_SESSION['AllDAta']['strtH'])){ ?>
                                              <option><?php echo $_SESSION['AllDAta']['strtH']; unset($_SESSION['AllDAta']['strtH']); ?></option> 
                                                 <?php } ?>
                                                       <option><?php echo $rowdata['start_time'];?></option>
                                                         <option>12</option>
                                                         <option>11</option>
                                                         <option>10</option>
                                                         <option>9</option>
                                                          <option>8</option>
                                                          <option>7</option>
                                                          <option>6</option>
                                                          <option>5</option>
                                                          <option>3</option>
                                                          <option>2</option>
                                                          <option>1</option>
                                                    </select>
                                                    <label>Munnite</label>
                                                    <select name="strtM">
                                                        <?php if(isset($_SESSION['AllDAta']['strtM'])){ ?>
                                              <option><?php echo $_SESSION['AllDAta']['strtM']; unset($_SESSION['AllDAta']['strtM']); ?></option> 
                                                 <?php } ?>
                                                            <option value="0">Minute</option>
                                                             <option>00</option>
                                                             <option>10</option>
                                                             <option>15</option>
                                                             <option>30</option>
                                                              <option>40</option>
                                                              <option>45</option>
                                                              <option>50</option>
                                                              <option>59</option>
                                                        </select>
                                                </td>
                                                
                                                <td><label>[End Time]<span style="color:red">*</span></label><br>
                                                 H:
                                                    <select name="endH">
                                                         <?php if(isset($_SESSION['AllDAta']['endH'])){ ?>
                                                            <option><?php echo $_SESSION['AllDAta']['endH']; unset($_SESSION['AllDAta']['endH']); ?></option> 
                                                         <?php } ?>
                                                            <option value="0"><?php echo $rowdata['ending_time'];?></option>
                                                         <option>12</option>
                                                         <option>11</option>
                                                         <option>10</option>
                                                         <option>9</option>
                                                          <option>8</option>
                                                          <option>7</option>
                                                          <option>6</option>
                                                          <option>5</option>
                                                          <option>3</option>
                                                          <option>2</option>
                                                          <option>1</option>
                                                    </select>
                                                    Munite
                                                    <select name="endM">
                                                         <?php if(isset($_SESSION['AllDAta']['endM'])){ ?>
                                                            <option><?php echo $_SESSION['AllDAta']['endM']; unset($_SESSION['AllDAta']['endM']); ?></option> 
                                                         <?php } ?>
                                                            <option value="0">Minute</option>
                                                             <option>00</option>
                                                             <option>10</option>
                                                             <option>15</option>
                                                             <option>30</option>
                                                              <option>40</option>
                                                              <option>45</option>
                                                              <option>50</option>
                                                              <option>59</option>
                                                        </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table></td>
                     <!--??/--<td colspan="3"><input type="submit" value="Submit"/><input type="reset" value="Reset"/></td>-->
<!--                     <tr>
                          <td colspan="3">
                              <h2 style="background:none; ">
                                  <span style="float: right; margin-right: 50px;"><input type="button" value="Edit" onclick="window.location.href='./ScheduleEdit.php?unique_id=<?php $data['unique_id']; ?>'"></span>
                                  <span style="float: right; margin-right: 50px;"><input type="button" value="Delete" onclick="window.location.href='./ScheduleDelete.php?unique_id=<?php $data['unique_id']; ?>'"></span>
                                  <span style="float: right; margin-right: 50px;"><input type="button" value="Back" onclick="window.location.href='./Overview.php'"></span>
                                  
                                  
                              </h2>
                          </td>
                      </tr>-->
                      
                      </tr>
                      
                    </table>

                </form>
                <!--<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>-->
      </div>
 </div>
<?php include '../inc/footer.php';?>
            <script>
                    function getId(val){
                    $.ajax({
                          type: "post",
                          url: "TrainerAjax.php",
                          data: "courses_id="+val,
                          success: function (data){
                              $("#assTrainer").html(data);
                          }
                           
                          });
                    }
                    </script>
               