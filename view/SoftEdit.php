<?php 
include_once '../vendor/autoload.php';

use labApps\Lab\Software\Software;


$object=new Software();

$data=$object->prepare($_GET)->Soft_show();

include '../inc/header.php'; 

?> 

<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="UserAdd.php">New Software</a></li>
                <li class="ic-grid-tables"><a href="SoftList.php"><span></span>View All Instalation Softwer</a></li>
                <li class="ic-charts"><a href="#"><span>Visit Website</span></a></li>
            </ul>
 </div>
<?php 
include '../inc/sidebar.php';
 
 
?>

<div class="grid_10">
            <div class="box round first grid">
                <h2>Update Software</h2>
               <div class="block copyblock"> 
                   <form action="SoftUpdate.php" method="POST">
                    <table class="form">
                        <tr>
                            <?php if(isset($_SESSION['error_msg'])) { echo $_SESSION['error_msg']; unset($_SESSION['error_msg']); } ?>
                        </tr>
                        <tr>
                            <td>Software Title<span style="color: red;">*</span> <br><input type="text" name="SoftName" 
                                value="<?php if(isset($data['software_title'])) { echo $data['software_title']; unset($data['software_title']); } if(isset($_SESSION['AllDAta']['SoftName'])){ echo $_SESSION['AllDAta']['SoftName']; unset($_SESSION['AllDAta']['SoftName']); } ?>" <?php if(isset($_SESSION['fulN_emt_msg'])) { echo $_SESSION['fulN_emt_msg']; unset($_SESSION['fulN_emt_msg']); } ?></td>
                        </tr>
                         <tr>
                            <td>Version<span style="color: red;">*</span> <br><input type="text" name="versionName" 
                              value="<?php if(isset($data['version'])) { echo $data['version']; unset($data['version']); } if(isset($_SESSION['AllDAta']['versionName'])){ echo $_SESSION['AllDAta']['versionName']; unset($_SESSION['AllDAta']['versionName']); } ?>" <?php if(isset($_SESSION['u_emt_msg'])) { echo $_SESSION['u_emt_msg']; unset($_SESSION['u_emt_msg']); } ?>
                            
                            </td>
                            <td><input type="hidden" name="unique_id" 
                              value="<?php if(isset($data['unique_id'])) { echo $data['unique_id']; unset($data['unique_id']); } if(isset($_SESSION['AllDAta']['unique_id'])){ echo $_SESSION['AllDAta']['unique_id']; unset($_SESSION['AllDAta']['unique_id']); } ?>" <?php if(isset($_SESSION['u_emt_msg'])) { echo $_SESSION['u_emt_msg']; unset($_SESSION['u_emt_msg']); } ?></td>
                        </tr>
                         <tr>
                             <td>Software Type<span style="color: red;">*</span><br>
                                 <select autocomplete="a" name="type">
                                    <option>Select Software Type</option>
                                     <option value="Free">Free Software</option>
                                    <option value="Premium">Premium Software</option>
                                  </select>
                             </td>
                        </tr>
                        
                         <tr>
                             <td>Lab Name<span style="color: red;">*</span><br>
                                 <select autocomplete="a" name="lab">
                                    <option>Select Leb Type</option>
                                     <option value="1">PHP **301**</option>
                                    <option value="2">ASP **302**</option>
                                    <option value="3">Java **303**</option>
                                  </select>
                             </td>
                        </tr>
                          
			<tr> 
                            <td>
                                <input type="submit" Value="Insert" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>

<?php include '../inc/footer.php';?>