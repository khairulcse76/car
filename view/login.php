<?php

include_once '../vendor/autoload.php';

use labApps\Lab\User\users;

$object=new users();

    if(isset($_SESSION['user']))
{
    $_SESSION['login_msg']='<span style="color: red; font-size:16px; ">please logout first</span>';
     header('location:dashboard.php');
} else{
  if($_SERVER['REQUEST_METHOD']=='POST')
  {
   $username=$_POST['username'];
   $password=$_POST['password'];
   
   if(empty($username) || empty($password))
   {
       $_SESSION['err_msg']= "Field must be not empty..!";
   }  else {
       $loginchk=$object->prepare($_POST)->login();
       
       if($loginchk)
       {
           $_SESSION['login_msg']='<b style=" color:blue; font-size: 16px; ">Login Successfull';
           header('location:dashboard.php');
       }  else {
       
           $_SESSION['err_msg']='<b style=" color:blue; font-size: 16px; ">user name or password dose not match..!!';
       }
    }
  }
}
?>


<head>
<meta charset="utf-8">
<title>Login</title>
    <link rel="stylesheet" type="text/css" href="stylelogin.css" media="screen" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
	<section id="content">
		<form action="" method="post">
			<h1>Admin Login</h1>
                       <?php if(isset($_SESSION['err_msg'])) { ?>
                        <p style="color:red;"><?php echo $_SESSION['err_msg']; unset($_SESSION['err_msg']); ?></p>
                         <?php } ?>
			<div>
				<input type="text" placeholder="Username"  name="username"/>
			</div>
			<div>
				<input type="password" placeholder="Password"  name="password"/>
			</div>
			<div>
                <input type="submit" value="Log in" /><a href="Register.php">Sing up</a>
			</div>
		</form><!-- form -->
		<div class="button">
			<a href="#">BACKS\ASH</a>
		</div>
	</section>
</div>
</body>
</body>
</html>
