﻿<?php session_start();
    include 'inc/header.php';
    include 'inc/sidebar.php';
    
    include_once '../vendor/autoload.php';
      
    use ecommerce\product;
     use ecommerce\Helper\Format;
    
     $catlistObj=new product();
     $data=$catlistObj->productview();
     $format=new Format();
     
     
     
;?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Post List</h2>
        <div class="block">  
            <table class="data display datatable" id="example">
			<thead>
				<tr>
					<th>Id</th>
					<th>Product Name</th>
					<th>Category</th>
					<th>Brand</th>
					<th>Description</th>
                                        <th>Price</th>
					<th>Image</th>
					<th>Type</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
                            
                            <?php 
                            $id=1;
                            foreach ($data as $value){ ?>
                               
                            <tr class="odd gradeX">
					<td><?php echo $id++; ?></td>
					<td><?php echo $value['productName']; ?></td>
                                        <td><?php echo $value['catName']; ?></td>
                                        <td><?php echo $value['BrandsName']; ?></td>
                                        <td><?php echo $format->textShorten( $value['description'], 50); ?></td>
                                        <td>$<?php echo $value['price']; ?></td>
                                        <td><img src="<?php echo $value['image']; ?>" width="60" height="40" /></td>
                                        <td><?php echo $value['type']; ?></td>
                                        <td><a href="editProduct.php?unique_id=<?php echo $value['unique_id']; ?>">Edit</a> ||
                                            <a href="deleteProduct.php?unique_id=<?php echo $value['unique_id']; ?>">Delete</a></td>
				</tr>
                            
                            
                          <?php  }
                            ?>
				
				
			</tbody>
		</table>

       </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();.0
		setSidebarHeight();
    });
</script>
<?php include 'inc/footer.php';?>
